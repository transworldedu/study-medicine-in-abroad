<!doctype html>
<html class="no-js" lang="en">
<?php require_once('common.php');?>
<?php include('header.php');?>
<body>
    <main>
        <div class="bannerWrap">
            <div class="overlay"></div>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-9">
                        <div class="imgWrap">
                            <img src="assets/img/banner-img-1.png" class="img-fluid" alt="DMSF"/>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-5 posLeft">
                        <div class="infobar infobarleft">
                            <ul>
                                <li>
                                    <div class="imgWrap imgMail">
                                        <img src="assets/img/ico-mail.png" class="img-fluid" alt="DMSF"/>
                                    </div>
                                    <div class="dataWrap">
                                        <a href="mailto:info@dmsf.ph">info@dmsf.ph</a>
                                    </div>
                                </li>
                                <li>
                                    <div class="imgWrap imgCall">
                                        <img src="assets/img/ico-call.png" class="img-fluid" alt="DMSF"/>
                                    </div>
                                    <div class="dataWrap">
                                        <a href="tel:1800-120-7484">1800 120 7484</a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4 logoWrap">
                        <div class="imgWrap">
                            <a href="https://www.dmsf.ph/" target="_blank">
                                 <img src="assets/img/logo.png" class="img-fluid" alt="DMSF"/>
                            </a>
                         </div>
                    </div>
                    <div class="col-lg-4 col-md-5 posRight">
                        <div class="infobar infoWrapRight">
                            <div class="dataWrap">
                                <h1>dmsf</h1>
                                <p>Davao City, Philippines</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="formWrapper">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-10">
                        <div class="row align-items-center">
                            <div class="col-md-6 d-flex justify-content-end">
                                <div class="formWrap">
                                    <h3>Want to study MBBS abroad?</h3>
                                    <p>Submit your details and get a free counselling call today!</p>
                                    <form>
                                        <ul class="formList">
                                            <li>
                                                <div class="inputField">
                                                    <span class="flaticon-user-profile"></span>
                                                    <input type="text" id = "name" placeholder="Name"/>
                                                </div>
                                            </li>
                                           
                                        <li>
                                            <div class="inputField">
                                                <span class="flaticon-envelope-1"></span>
                                                  <input id="email" type="email" name="email" placeholder="Email" required/>
                                                  
                                            </div>
                                            <span class="error_form" id="email_error_message" style="color: white;display: inline;"></span>
                                        </li>
                                        <li>
                                            <div class="inputField">
                                                <span class="flaticon-phone"></span>
                                                 <input id="mobile_number" type="text" name="mobile_number" pattern="[1-9]{1}[0-9]{9}"  onKeyUp="numericFilter(this);" maxlength="10" pattern="\d{10}" title="Please enter exactly 10 digits" placeholder="Mobile Number" required /> 
                                            </div>
                                             <span class="error_form" id="password_error_message" style="color: white;display: inline;" ></span>
                                        </li>
                                            <li>
                                                <div class="inputField">
                                                    <span class="flaticon-calendar-1"></span>
                                                    <div class="selectWrap">
                                                        <select id = "appearing_year">
                                                            <option value  = "1">NEET Appearing Year</option>
                                                            <option value = "2020">2020</option>
                                                            <option value = "2019">2019</option>
                                                            <option value = "2018">2018</option>
                                                            <option value = "2017">2017</option>
                                                            <option value = "2016">2016</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="ctaWrap">
                                                    <button type="button" name = "submit_details" class="btn-submit btn">Submit</button>
                                                </div>
                                            </li>
                                        </ul>
                                    </form>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="dataWrap">
                                    <h2><span>Study Medicine Abroad</span> Be a part of DMSF, the best college to study MBBS abroad.</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="whydmsfWrap">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-sm-12">
                        <div class="titleWrap">
                            <h2>Why the Philippines is the best <br/> country to study MBBS abroad?</h2>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <ul class="featuresListing">
                            <li>
                                <div class="iconWrap">
                                    <img src="assets/img/language.png" alt="DMSF" class="img-fluid"/>
                                </div>
                                <div class="dataWrap">
                                    <h2>Safe & Secured For Students</h2>
                                    <p>The Philippines is the country tailor-made for student life.</p>
                                </div>
                            </li>
                            <li>
                                <div class="iconWrap">
                                    <img src="assets/img/quality.png" alt="DMSF" class="img-fluid"/>
                                </div>
                                <div class="dataWrap">
                                    <h2>Low-Cost High-Quality Education</h2>
                                    <p>Studying Medicine abroad in the Philippines is easily affordable.</p>
                                </div>
                            </li>
                            <li>
                                <div class="iconWrap">
                                    <img src="assets/img/hand.png" alt="DMSF" class="img-fluid"/>
                                </div>
                                <div class="dataWrap">
                                    <h2>Intense exposure on hands-on practice</h2>
                                    <p>Study MBBS abroad with the best facilities and practices.</p>
                                </div>
                            </li>
                            <li>
                                <div class="iconWrap">
                                    <img src="assets/img/Students.png" alt="DMSF" class="img-fluid"/>
                                </div>
                                <div class="dataWrap">
                                    <h2>Student-friendly American pattern</h2>
                                    <p>Best curriculum to study medicine abroad. </p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="whyAbroadWrap">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="titleWrap">
                            <h2>Why study medicine abroad?</h2>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-md-11">
                        <div class="row forMobile">
                            <div class="col-md-7">
                                <div class="dataWrap">
                                    <p>The Philippines for over the last 20 years has been the best place to study medicine abroad. Although govt colleges in India are highly recognised, the unbalanced ratio of the number of applicants to the number of seats available makes it very difficult for the students to get into reputed medical colleges. With an average NEET score, it is difficult for a student to get into the govt colleges. And the challenge with the private colleges is their cost of education which is significantly higher than the govt colleges.</p>
                                    <p>DMSF in the Philippines offers direct admission for all. That too, at a very low cost without compromising on the quality of education. DMSF has tailored its curriculum and has become themost opted college to study medicine abroad for Indian students. The country has a low-expense lifestyle and hence is ideal for the students to study medicine abroad.</p>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="imgWrap">
                                    <img src="assets/img/why-abroad-img.jpg" alt="DMSF" class="img-fluid"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container width100pr">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="titleWrap innerTitle">
                            <h2>Challenges of Studying Medicine Abroad</h2>
                            <p>And our tailor-made solutions for the students</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="sliderDataWrap">
                            <div class="row justify-content-center">
                                <div class="col-md-6">
                                    <div class="silderttl">
                                        <p>There are quite a few factors that the students and the parents must consider while choosing a country to study medicine abroad:</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <div class="col-md-9">
                                    <ul class="studyMedicineList">
                                        <li>
                                            <div class="iconWrap">
                                                <img src="assets/img/language_2.png" alt="DMSF" class="img-fluid"/>
                                            </div>
                                            <div class="dataWrap">
                                                <h2>Language Barrier</h2>
                                                <p>The Philippines is the 3rd largest English speaking country. Hence, students do not face any language barrier. Studying abroad in other countries might require learning the language because, during clinical rotations, students will have to interact with patients and local doctors. And English is the dominant language in the Philippines. Thus, it is the best country to study medicine abroad.</p>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="iconWrap">
                                                <img src="assets/img/culture.png" alt="DMSF" class="img-fluid"/>
                                            </div>
                                            <div class="dataWrap">
                                                <h2>Cultural Trends</h2>
                                                <p>To study medicine abroad, students should consider factors like food habits. The Philippines has a similar cultural trend as India and hence are custom-made for Indian students to study medicine abroad. This reduces the feeling of homesickness too.</p>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="iconWrap">
                                                <img src="assets/img/dieses.png" alt="DMSF" class="img-fluid"/>
                                            </div>
                                            <div class="dataWrap">
                                                <h2>Similar Disease Spectrum</h2>
                                                <p>One of the crucial factors is the disease spectrum of the foreign country where the student is pursuing medicine abroad. Most of the students after completing their degrees, usually prefer to return to their homeland to practice. Therefore, it becomes vital for the students to practice medicine on the spectrum of the same disease as they would encounter in their native land. The Philippines and India share a similar disease spectrum and hence aids the students in the latter stages of theircareer as well.</p>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="iconWrap">
                                                <img src="assets/img/pracitce.png" alt="DMSF" class="img-fluid"/>
                                            </div>
                                            <div class="dataWrap">
                                                <h2>Preparing for separate licensure examinations for practice</h2>
                                                <p>For a student who opts to study medicine abroad, it is required for the student to clear a licensure examination to obtain the license for the practice. DMSF provides dedicated <a href="https://nbe.edu.in/" target="_blank">FMGE</a>  coaching in association with Transworld Educare Pvt Ltd for the Indian students to help them with  their licensure examination. </p>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="iconWrap">
                                                <img src="assets/img/expences.png" alt="DMSF" class="img-fluid"/>
                                            </div>
                                            <div class="dataWrap">
                                                <h2>Cost & Expenses</h2>
                                                <p>One of the major concerns among the parents is the cost of education. DMSF provides low-cost high-quality medical education for all those who want to study medicine abroad. Also, the cost of living in the Philippines is considerably high compared to some other European and North American countries. Furthermore, to study MBBS abroad in the Philippines, there are numerous scholarships for Indian students to study medicine abroad.</p>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="benifitsWrap">
            <div class="container width100pr">
                <div class="row justify-content-center">
                    <div class="col-md-9">
                        <div class="topSectionWrap">
                            <h2>Benefits</h2>
                            <h3>DMSF is the best medical college<br/>
                                to study medicine abroad.
                                </h3>
                            <p>Here’s why</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <ul class="benifitsList">
                            <li>
                                <div class="iconWrap">
                                    <img src="assets/img/ico-1.png" class="img-fluid" alt="DMSF"/>
                                </div>
                                <div class="dataWrap">
                                    <h2>Prepare for competitive <br/> exams with the base <br/> curriculum.</h2>
                                </div>
                            </li>
                            <li>
                                <div class="iconWrap">
                                    <img src="assets/img/ico-2.png" class="img-fluid" alt="DMSF"/>
                                </div>
                                <div class="dataWrap">
                                    <h2>Get renowned <br/> recommendations. </h2>
                                </div>
                            </li>
                            <li>
                                <div class="iconWrap">
                                    <img src="assets/img/ico-3.png" class="img-fluid" alt="DMSF"/>
                                </div>
                                <div class="dataWrap">
                                    <h2>MCI & WHO  <br/> approved courses.</h2>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="studentLifeWrap">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-sm-12">
                        <div class="titleWrap">
                            <h2>Student Life</h2>
                        </div>
                    </div>
                    <div class="col-md-11">
                        <ul class="studentlifeListing galleryImg pretty">
                            <li>
                                <div class="imgWrap">
                                    <img src="assets/img/gallery/gal-1.jpg" alt="Davao Medical School Foundation-DMSF Gallery" class="img-fluid"/>
                                    <div class="icon">
                                    <a href="assets/img/gallery/gal-1.jpg" rel="prettyPhoto[gallery2]"><span class="flaticon-search"></span></a>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="imgWrap">
                                    <img src="assets/img/gallery/gal-2.jpg" alt="Davao Medical School Foundation-DMSF Gallery" class="img-fluid"/>
                                    <div class="icon">
                                    <a href="assets/img/gallery/gal-2.jpg" rel="prettyPhoto[gallery2]"><span class="flaticon-search"></span></a>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="imgWrap">
                                    <img src="assets/img/gallery/gal-3.jpg" alt="Davao Medical School Foundation-DMSF Gallery" class="img-fluid"/>
                                    <div class="icon">
                                    <a href="assets/img/gallery/gal-3.jpg" rel="prettyPhoto[gallery2]"><span class="flaticon-search"></span></a>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="imgWrap">
                                    <img src="assets/img/gallery/gal-4.jpg" alt="Davao Medical School Foundation-DMSF Gallery" class="img-fluid"/>
                                    <div class="icon">
                                    <a href="assets/img/gallery/gal-4.jpg" rel="prettyPhoto[gallery2]"><span class="flaticon-search"></span></a>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="imgWrap">
                                    <img src="assets/img/gallery/gal-5.jpg" alt="Davao Medical School Foundation-DMSF Gallery" class="img-fluid"/>
                                    <div class="icon">
                                    <a href="assets/img/gallery/gal-5.jpg" rel="prettyPhoto[gallery2]"><span class="flaticon-search"></span></a>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="imgWrap">
                                    <img src="assets/img/gallery/gal-6.jpg" alt="Davao Medical School Foundation-DMSF Gallery" class="img-fluid"/>
                                    <div class="icon">
                                    <a href="assets/img/gallery/gal-6.jpg" rel="prettyPhoto[gallery2]"><span class="flaticon-search"></span></a>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="imgWrap">
                                    <img src="assets/img/gallery/gal-7.jpg" alt="Davao Medical School Foundation-DMSF Gallery" class="img-fluid"/>
                                    <div class="icon">
                                    <a href="assets/img/gallery/gal-7.jpg" rel="prettyPhoto[gallery2]"><span class="flaticon-search"></span></a>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="imgWrap">
                                    <img src="assets/img/gallery/gal-8.jpg" alt="Davao Medical School Foundation-DMSF Gallery" class="img-fluid"/>
                                    <div class="icon">
                                    <a href="assets/img/gallery/gal-8.jpg" rel="prettyPhoto[gallery2]"><span class="flaticon-search"></span></a>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="imgWrap">
                                    <img src="assets/img/gallery/gal-9.jpg" alt="Davao Medical School Foundation-DMSF Gallery" class="img-fluid"/>
                                    <div class="icon">
                                    <a href="assets/img/gallery/gal-9.jpg" rel="prettyPhoto[gallery2]"><span class="flaticon-search"></span></a>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="imgWrap">
                                    <img src="assets/img/gallery/gal-10.jpg" alt="Davao Medical School Foundation-DMSF Gallery" class="img-fluid"/>
                                    <div class="icon">
                                    <a href="assets/img/gallery/gal-10.jpg" rel="prettyPhoto[gallery2]"><span class="flaticon-search"></span></a>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="imgWrap">
                                    <img src="assets/img/gallery/gal-11.jpg" alt="Davao Medical School Foundation-DMSF Gallery" class="img-fluid"/>
                                    <div class="icon">
                                    <a href="assets/img/gallery/gal-11.jpg" rel="prettyPhoto[gallery2]"><span class="flaticon-search"></span></a>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="imgWrap">
                                    <img src="assets/img/gallery/gal-12.jpg" alt="Davao Medical School Foundation-DMSF Gallery" class="img-fluid"/>
                                    <div class="icon">
                                    <a href="assets/img/gallery/gal-12.jpg" rel="prettyPhoto[gallery2]"><span class="flaticon-search"></span></a>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <?php $base_url = $common_function->base_url();?>
    <?php include('footer.php')?>
    <!-- Scroll Up -->
    <div id="back-top">
        <a title="Go to Top" href="#"> <i class="fas fa-level-up-alt"></i></a>
    </div>
    <script src="assets/js/jquery-1.12.4.min.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/slick.min.js"></script>
    <script src="assets/js/main.js"></script>
    <script src="assets/js/camera.min.js"></script>
    <script>
         jQuery(function(){
                 jQuery('#camera_wrap_1').camera({
                 transPeriod: 500,
                 time: 3000,
                 height: '490px',
                 thumbnails: false,
                 pagination: true,
                 playPause: false,
                 loader: false,
                 navigation: false,
                 hover: false
             });
         });
    </script>
    <script src="assets/js/jquery.prettyPhoto.js"></script>
    <script>
         jQuery(document).ready(function(){
         	jQuery(".pretty a[rel^='prettyPhoto']").prettyPhoto({animation_speed:'normal',theme:'light_square',slideshow:3000, autoplay_slideshow: false, social_tools: ''});		
         });      
    </script>
    <script>
          jQuery(document).ready(function($){
            function scrollToSection(event) {
                event.preventDefault();
                var $section = $($(this).attr('href')); 
                $('html, body').animate({
                scrollTop: $section.offset().top
                }, 500);
            }
            $('[data-scroll]').on('click', scrollToSection);

            $('.btn-submit').click(function(){
                var  name           = $('#name').val();
                var  email          = $('#email').val();  
                var  mobile_number  = $('#mobile_number').val();
                var  appearing_year = $('#appearing_year').children("option:selected").val();
                    if(name == "" ){
                        swal({title: "Opps", text: "Please Fill The Name!", type: "warning"});
                        return false;
                    } 
                    if(email == ""){
                        swal({title: "Opps", text: "Please Fill The Email Address!", type: "warning"});
                        return false;
                    } 
                     if(ValidatEmail(email)==false){
                      swal({title: "Opps", text: "Please Fill The Valid Email Address!", type: "warning"});
                      return false;
                    }
                    if(mobile_number.length != 10 || mobile_number.length < 10 || mobile_number.length > 10 ){
                        swal({title: "Opps", text: "Please Fill The Valid Mobile Number!", type: "warning"});
                        return false;
                    } 
                    if(appearing_year == ""){
                        swal({title: "Opps", text: "Please Fill The Appearing Year!", type: "warning"});
                        return false;
                    } 

                    $.ajax({
                    url: "action.php",
                    type: 'POST',
                    data: { name  : name,
                            email : email,
                            mobile_number :mobile_number,
                            appearing_year:appearing_year
                         },
                        success: function(data){
                          var obj =  JSON.parse(data);
                          console.log(obj);
                          if(obj.response == "success"){
                            swal({title: "Good job", text: "Your Details Has been succefullt Submitted", type: "success"},
                                 function(){ 
                                     location.reload();
                                 }
                              );
                          }else{
                                swal({title: "Opps", text: "Your Details Is Not Submitted ...!", type: "warning"},
                                 function(){ 
                                     location.reload();
                                 }
                              );
                          }
                          
                        },

                      });  
                    false;    
                
                
            });
            function ValidatEmail(email) {
              var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
              if(!regex.test(email)) {
                return false;
              }else{
                return true;
              }
            }
        }(jQuery));
    </script>
     <script src="//code.tidio.co/okfnqicrqc450gbjmd2rdi0wfvmrfdwf.js" async></script>
     
        <script type="text/javascript">
      $(function() {

        
         $("#email_error_message").hide();
         $("#password_error_message").hide();
        

        
         var error_email = false;
         var error_password = false;
         
        
         $("#email").focusout(function() {
            check_email();
         });
         $("#mobile_number").focusout(function() {
            check_password();
         });
        

        

         function check_password() {
            var password_length = $("#mobile_number").val().length;
            if (password_length < 10) {
               $("#password_error_message").html("Atleast 10 Characters");
               $("#password_error_message").show();
               $("#mobile_number").css("border-bottom","2px solid #F90A0A");
               error_password = true;
            } else {
               $("#password_error_message").hide();
               $("#mobile_number").css("border-bottom","2px solid #34F458");
            }
         }

        

         function check_email() {
            var pattern = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/;
            var email = $("#email").val();
            if (pattern.test(email) && email !== '') {
               $("#email_error_message").hide();
               $("#email").css("border-bottom","2px solid #34F458");
            } else {
               $("#email_error_message").html("Invalid Email");
               $("#email_error_message").show();
               $("#email").css("border-bottom","2px solid #F90A0A");
               error_email = true;
            }
         }

         $("#registration_form").submit(function() {
           
            error_email = false;
            error_password = false;
           
           
            check_email();
            check_password();
           
            if (error_email === false && error_password === false) {
              // alert("Registration Successfull");
               return true;
            } else {
               alert("Please Fill the form Correctly");
               return false;
            }



         });
      });
      
       function numericFilter(txb) {
   txb.value = txb.value.replace(/[^\0-9]/ig, "");
}
   </script>
</body>
</html>