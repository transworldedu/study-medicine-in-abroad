<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?php  if($common_function->title() != NULL){echo $common_function->title();}else{echo "No Title";}?></title>
    <meta name="description" content="What are the most important factors one should consider to study medicine abroad? Find out why DMSF is the best college to study medicine abroad.">
    <meta name="keywords" content="Study Medicine Abroad
    Studying medicine abroad, study MBBS abroad, best place to study medicine abroad, study medicine for Indian students, scholarships for Indian students to study medicine abroad.">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/png" href="assets/img/favicon.png">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/slick.css">
    <link rel="stylesheet" href="assets/css/flaticon.css">
    <link rel="stylesheet" href="assets/css/fontawesome-all.min.css">
    <link rel='stylesheet' id='camera-css' href='assets/css/camera.css'>
    <link rel="stylesheet" href="assets/css/prettyPhoto.css" />
    <link rel="stylesheet" href="assets/css/style.css?v=1">
    <link rel="stylesheet" href="assets/css/device.css?v=1">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
 
</head>
