(function ($)
  { "use strict"

  // Scroll Up
    $('#back-top a').on("click", function () {
      $('body,html').animate({
        scrollTop: 0
      }, 800);
      return false;
    });

/* 7. Testimonials Slider*/
    $('.studyMedicineList').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        infinite: true,
        arrows: false,
        autoplay: true,
        speed: 800
    });

/* 8. Material Design Form */
  const setActive = (el, active) => {
    const formField = el.parentNode.parentNode;
    if (active) {
        formField.classList.add("form-field--is-active");
    } else {
        formField.classList.remove("form-field--is-active");
        el.value === "" ?
        formField.classList.remove("form-field--is-filled") :
        formField.classList.add("form-field--is-filled");
    }
    };

    [].forEach.call(
    document.querySelectorAll(".form-field__input, .form-field__textarea"),
    el => {
    el.onblur = () => {
        setActive(el, false);
    };
    el.onfocus = () => {
        setActive(el, true);
    };
    });

    $('.studentlifeListing').slick({
        dots: false,
        arrows: true,
        infinite: true,
        autoplay: true,
        speed: 800,
        slidesToShow: 4,
        slidesToScroll: 1,
        responsive: [
            {
            breakpoint: 992,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
                infinite: true,
                dots: false
            }
            },
            {
            breakpoint: 768,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1
            }
            },
            {
              breakpoint:640,
              settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1
            }
            },
        ]
    });

    /* 9. Gallery */
    $('.grid').masonry({
      itemSelector: '.grid-item',
      columnWidth: '.grid-sizer',
      percentPosition: true
    });
    

})(jQuery);


