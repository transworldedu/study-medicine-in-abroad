<footer>
        <!--? Footer Start-->
        <div class="footer-area footer-bg">
            <div class="container width100">
                <div class="footer-top footer-padding">
                    <!-- footer Heading -->
                    <div class="footer-heading">
                        <div class="row justify-content-center">
                            <div class="col-lg-5 col-sm-12">
                                <div class="footer-tittle2">
                                    <h4>Let’s Get Social</h4>
                                </div>
                                <!-- Footer Social -->
                                <div class="footer-social">
                                    <a href="https://www.facebook.com/DMSFofficial" target="_blank" class="facebook"><i class="fab fa-facebook-f"></i></a>
                                    <a href="https://twitter.com/DMSFofficial" target="_blank" class="twitter"><i class="fab fa-twitter"></i></a>
                                    <a href="https://www.youtube.com/channel/UCOUAJmOQy9wBolhuj7OhdEg" target="_blank" class="youtube"><i class="fab fa-youtube"></i></a>
                                    <a href="https://www.instagram.com/dmsf_india/" target="_blank" class="instagram"><i class="fab fa-instagram"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-lg-8">
                                <div class="divider div-transparent"></div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="footerAddress text-right rightDivider">
                                            <div itemscope itemtype="https://schema.org/Organization">
                                                <p><span itemprop="name"><strong>N.M. Wadia Hospital Campus</strong></span></p>
                                                <div itemprop="address" itemscope itemtype="https://schema.org/PostalAddress">
                                                    <p>
                                                    <span itemprop="streetAddress">283, Shukarwar Peth,</span> 
                                                    <span itemprop="addressLocality">Pune, <br/> Maharashtra</span> 
                                                    <span itemprop="postalCode">411002</span>
                                                    </p>
                                                </div>               
                                                <ul>
                                                    <li><span itemprop="email"><a href="mailto:info@dmsf.ph">info@dmsf.ph</a></span></li>
                                                    <li><span itemprop="telephone"><a href="tel:1800 120 7484">1800 120 7484</a></span></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="footerAddress text-left">
                                            <div itemscope itemtype="https://schema.org/Organization">
                                                <p><span itemprop="name"><strong>Davao Medical School Foundation, Inc</strong></span></p>
                                                <div itemprop="address" itemscope itemtype="https://schema.org/PostalAddress">
                                                    <p>
                                                        <span itemprop="streetAddress">DMSF Drive, Bajada,</span> 
                                                        <span itemprop="addressLocality">Davao City,<br/> Philippines.</span> 
                                                    </p>
                                                </div>               
                                                <ul>
                                                    <li><span itemprop="email"><a href="mailto:info@dmsf.ph">info@dmsf.ph</a></span></li>
                                                    <li><span itemprop="telephone"><a href="tel:+63 82 227 9330">+63 82 227 9330</a></span></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footerMenu">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-11">
                        <ul>
                            <li><a href="https://www.dmsf.ph/privacy-policy" target="_blank">PRIVACY POLICY</a></li>
                            <li><a href="https://www.dmsf.ph/terms-and-conditions" target="_blank">TERMS & CONDITION</a></li>
                            <li><a href="https://www.dmsf.ph/faq" target="_blank">FAQ'S</a></li>
                            <li><a href="https://www.dmsf.ph/disclaimer" target="_blank">DISCLAIMER</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>